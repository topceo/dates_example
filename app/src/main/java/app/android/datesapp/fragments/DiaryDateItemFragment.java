package app.android.datesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import app.android.datesapp.R;
import app.android.datesapp.utils.DateHelper;

public class DiaryDateItemFragment extends Fragment {

    public static final String SELECTED_DATE = "selected_date";

    String selectedDate;
    Context mContext;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.diary_pager_item, container, false);

        Bundle args = getArguments();
        mContext = getActivity();

        selectedDate = args.getString(SELECTED_DATE);

        String dateTxt = DateHelper.getDateForHuman(selectedDate, mContext)
                + DateHelper.getDateSuffix(selectedDate, mContext);
        ((TextView) rootView.findViewById(R.id.diaryDateTxt)).setText(dateTxt);

        return rootView;
    }

}
