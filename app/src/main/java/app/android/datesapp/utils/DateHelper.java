package app.android.datesapp.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import app.android.datesapp.R;

public class DateHelper {

    public static String getDateForHuman(String date, Context ctx)
    {
        try {
            Date parsedDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(date);

            String month = new SimpleDateFormat("M").format(parsedDate);
            String day = new SimpleDateFormat("d").format(parsedDate);
            String year = new SimpleDateFormat("yyyy").format(parsedDate);

            String currentYear = new SimpleDateFormat("yyyy").format(new Date());

            int monthResourceId = ctx.getResources()
                    .getIdentifier("diary_month_" + month, "string", ctx.getPackageName());

            return day + " " + ctx.getString(monthResourceId) + (currentYear.equals(year) ? "" : " " + year);

        } catch (Exception e) {
            return date;
        }
    }

    public static String getCurrentDate()
    {
        Calendar calendar = new GregorianCalendar();
        // calendar.add(GregorianCalendar.DATE, -51);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(calendar.getTime());
    }

    public static String getYesterdayDate()
    {
        Calendar calendar = new GregorianCalendar();
        calendar.add(GregorianCalendar.DATE, -1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(calendar.getTime());
    }

    public static String getTomorrowDate()
    {
        Calendar calendar = new GregorianCalendar();
        calendar.add(GregorianCalendar.DATE, 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(calendar.getTime());
    }

    public static String getDateSuffix(String selectedDate, Context ctx)
    {
        String currDate = DateHelper.getCurrentDate();

        String suffix = " ";

        if (currDate.equals(selectedDate)) {
            suffix = suffix + ctx.getString(R.string.today);
        } else if (selectedDate.equals(DateHelper.getYesterdayDate())) {
            suffix = suffix + ctx.getString(R.string.yesterday);
        } else if (selectedDate.equals(DateHelper.getTomorrowDate())) {
            suffix = suffix + ctx.getString(R.string.tomorrow);
        }


        return suffix;
    }
}
