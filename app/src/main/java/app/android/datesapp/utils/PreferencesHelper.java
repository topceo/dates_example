package app.android.datesapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {

    static final String DIARY_DATE_KEY = "diary_date";

    SharedPreferences pref;
    SharedPreferences.Editor editPref;
    Context ctx;

    public PreferencesHelper(Context ctx) {
        this.ctx = ctx;
    }

    public void setDiaryDate(String date) {
        pref = ctx.getSharedPreferences("main", Context.MODE_PRIVATE);
        editPref = pref.edit();
        editPref.putString(DIARY_DATE_KEY, date);
        editPref.commit();
    }

    public String getDiaryDate() {
        pref = ctx.getSharedPreferences("main", Context.MODE_PRIVATE);
        String diaryDate = pref.getString(DIARY_DATE_KEY, "");
        if (diaryDate.equals("")) {
            diaryDate = DateHelper.getCurrentDate();
            setDiaryDate(diaryDate);
        }

        return diaryDate;
    }
}
