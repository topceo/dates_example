package app.android.datesapp.adapters;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

import app.android.datesapp.fragments.DiaryDateItemFragment;

public class DiaryPagerAdapter extends FragmentStatePagerAdapter {

    List<String> data;
    Context ctx;
    FragmentManager fm;

    public DiaryPagerAdapter(FragmentManager fm, Context ctx, List<String> data) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.data = data;
        this.ctx = ctx;
        this.fm = fm;
    }

    public String getPageDate(int i)
    {
        return data.get(i);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new DiaryDateItemFragment();

        Bundle args = new Bundle();
        args.putString(
                DiaryDateItemFragment.SELECTED_DATE,
                getPageDate(i)
        );

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Item " + (position + 1);
    }
}
