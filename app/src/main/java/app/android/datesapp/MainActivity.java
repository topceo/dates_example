package app.android.datesapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import app.android.datesapp.adapters.DiaryPagerAdapter;
import app.android.datesapp.utils.PreferencesHelper;

public class MainActivity extends AppCompatActivity {

    public static int DATES_PAGER_CNT = 2000;

    List<String> dates;
    ViewPager mViewPager;
    String selectedDate;
    PreferencesHelper prefHelper;
    DiaryPagerAdapter diaryPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dates = getDates();
        prefHelper = new PreferencesHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        selectedDate = prefHelper.getDiaryDate();

        mViewPager = findViewById(R.id.diaryPager);
        int currentItem = dates.indexOf(selectedDate);

        diaryPagerAdapter = new DiaryPagerAdapter(
                getSupportFragmentManager(),
                this,
                dates
        );

        mViewPager.setAdapter(diaryPagerAdapter);
        mViewPager.setCurrentItem(currentItem);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedDate = dates.get(position);
                prefHelper.setDiaryDate(selectedDate);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private List<String> getDates() {
        List<String> data = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.add(GregorianCalendar.DATE, -(DATES_PAGER_CNT / 2 + 1));
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        for (int i = 0; i < DATES_PAGER_CNT; i++) {
            calendar.add(GregorianCalendar.DATE, 1);
            String dt = format1.format(calendar.getTime());
            data.add(dt);
        }

        return data;
    }
}